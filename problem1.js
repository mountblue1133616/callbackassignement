const fs = require(`fs`);

//function for creating directory
const createAndDelete = (numberOfFiles, testDirectory) => {
  if (typeof testDirectory !== "string" || typeof numberOfFiles !== "number") {
    console.log("Incorrect input type)");
  } else {
    //Step 1:Create directory
    fs.mkdir(`./${testDirectory}`, { recursive: true }, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log("Created directory");
        createRandomJSONFiles(numberOfFiles, testDirectory); //callback for creating Random Files
      }
    });
  }
};

//function for creating random files

const createRandomJSONFiles = (numberOfFiles, testDirectory) => {
  let counter = 1;
  for (let i = 1; i <= numberOfFiles; i++) {
    let filename = `File${i}.txt`;
    console.log(filename);
    let filecontent = {
      i: Math.random(),
    };
    console.log(filecontent);
    fs.writeFile(
      `${testDirectory}/${filename}`,
      JSON.stringify(filecontent),
      (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log(`File ${i} created`);
          counter++;
          if (counter === numberOfFiles) {
            deleteFiles(numberOfFiles, testDirectory, "File"); //callback function
          }
        }
      }
    );
  }
};

//function for deleting files
const deleteFiles = (numberOfFiles, testDirectory, filename) => {
  for (let i = 1; i <= numberOfFiles; i++) {
    fs.unlink(`${testDirectory}/${filename}${i}.txt`, (err) => {
      if (err) {
        console.log(err);
      } else {
        console.log(`File ${i} deleted`);
      }
    });
  }
};

module.exports = createAndDelete;
