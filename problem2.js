const fs = require(`fs`);

const problem2 = (filePath, fileNamePath) => {
  fs.readFile(filePath, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      console.log("Read lipsum file successfully");
      let upperCase = data.toString().toUpperCase();
      let lowerCase = upperCase.toLowerCase().split(".");
      let sort = lowerCase.sort();

      fs.writeFile("./file1.txt", upperCase, (error) => {
        if (error) {
          console.log(error);
        } else {
          console.log("Written problem 2 Successfully");
          fs.appendFile("./filenames.txt", `file1.txt\n`, (error) => {
            if (error) {
              console.log(error);
            } else {
              console.log("Append of problem 2 successful");
            }
          });
        }
      });

      fs.writeFile("./file2.txt", lowerCase.join("\n"), (error) => {
        if (error) {
          console.log(error);
        } else {
          console.log("Written problem 3 successfully");
          fs.appendFile("filenames.txt", `file2.txt\n`, (error) => {
            if (error) {
              console.log(error);
            } else {
              console.log("Append of problem 3 successful");
            }
          });
        }
      });

      fs.writeFile("./file3.txt", sort.join("\n"), (error) => {
        if (error) {
          console.log(error);
        } else {
          console.log("Written problem 4 Successfully");
          fs.appendFile("filenames.txt", `file3.txt\n`, (error) => {
            if (error) {
              console.log(error);
            } else {
              console.log("Append problem 4 successful");
              // file deletion code moved here
              fs.readFile(fileNamePath, (error, data) => {
                if (error) {
                  console.log(erro);
                } else {
                  let fileNames = data.toString().split("\n");
                  let fileNamesTrim = fileNames.filter((value) => value);
                  fileNamesTrim.forEach((fileName, i) => {
                    fs.unlink(fileName, (err) => {
                      if (err) {
                        console.log(err);
                      } else {
                        console.log(`Deleted file${i + 1} successfully`);
                      }
                    });
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};

module.exports = problem2;
